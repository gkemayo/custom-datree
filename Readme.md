[[_TOC_]]

### 1. Exécuter une analyse Datree avec l'image officielle :

>>    `cat <mon_manifest>.yaml | docker run -i -e DATREE_TOKEN=$DATREETOKEN datree/datree test [--schema-location "custom_policies"] -`


### 2. Exécuter Datree avec son image officielle modifiée :

     => (voir le Dockerfile du repertoire /from_native_datree_img)

>>>    `cat <mon_manifest>.yaml | docker run -i gkemayo/datree test [--schema-location "custom_policies"] -`


### 3. Exécuter Datree avec une image debian dans laquelle elle a été installée :

     => (voir le Dockerfile du repertoire /with_a_debian_img)

>    * Méthode 1 (dans le container) :
>
>>>    `docker run -i -v <repertoire_vers_iac_kube>:/iac gkemayo/debian-datree`<br />
>>>    `datree test [--schema-location "custom_policies"] /iac/<mon_manifest>.yaml`  (dans le container)

>    * Méthode 2 (hors du container) :
>
>>>    `cat <mon_manifest>.yaml | docker run -i gkemayo/debian-datree datree test -`

>    * Méthode 3 (dans un GitLab-CI) :
>
>>>    `datree test [--schema-location "custom_policies"] <mon_manifest>.yaml` 

---     
<div align="center">
 <img width="460" height="300" src="./datree.png">

 [Datree website](https://www.datree.io/) 
 </div>
 
